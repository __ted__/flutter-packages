import 'dart:async';
import 'dart:math';
import 'dart:ui';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:geoflutterfire/geoflutterfire.dart';
import 'package:google_maps_cluster_manager/google_maps_cluster_manager.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:rxdart/rxdart.dart';
import 'utils/map_helper.dart';

typedef void MapCreated(
    GoogleMapController controller, LatLngBounds bounds, double metersPerPixel);

typedef void MapUpdate(CameraPosition position, double metersPerPixel);

// typedef List<ClusterItem<T>> ClusterBuilder<T>(List<DocumentSnapshot> data);

typedef Future<T> ItemBuilder<T>(List<DocumentSnapshot> data);
typedef void ItemTap<T>(T data);

typedef Future<BitmapDescriptor> ClusterDrawer<T>(Cluster<T> cluster);

class ExploreMapController extends ValueNotifier<Set<Marker>> {
  ExploreMapController({Set<Marker> value}) : super(value);
}

class ExploreMap<T> extends StatefulWidget {
  final CameraPosition initPosition;
  final MinMaxZoomPreference minMaxZoomPreference;
  final Set<Marker> markers;
  final Set<Circle> circles;
  final MapCreated onMapCreated;
  final ValueChanged<double> metersPerPixelUpdate;
  final ValueChanged<LatLngBounds> currentBoundsUpdate;
  final ClusterDrawer<T> clusterDrawer;
  final ItemBuilder<List<ClusterItem<T>>> clusterBuilder;
  final ItemBuilder<Set<Marker>> markerBuilder;
  final ItemBuilder<Set<Circle>> circleBuilder;
  final VoidCallback onCameraStart;
  final MapUpdate onCameraMove;
  final VoidCallback onCameraIdle;
  final ItemTap<T> onMarkerTap;
  final Query collection;
  final bool enableClusters;
  final LatLngBounds maxBounds;
  final ExploreMapController controller;

  ExploreMap(
      {this.controller,
      this.collection,
      this.clusterBuilder,
      this.markerBuilder,
      this.circleBuilder,
      this.clusterDrawer,
      this.initPosition,
      this.minMaxZoomPreference,
      this.markers,
      this.circles,
      this.onMapCreated,
      this.onCameraStart,
      this.onCameraMove,
      this.onCameraIdle,
      this.onMarkerTap,
      this.metersPerPixelUpdate,
      this.currentBoundsUpdate,
      this.maxBounds,
      this.enableClusters = false});

  @override
  ExploreMapState<T> createState() => ExploreMapState<T>();
}

class ExploreMapState<T> extends State<ExploreMap<T>> {
  ClusterManager<T> _manager;

  GoogleMapController _mapController;

  CameraPosition _cameraPosition;

  GeoFirePoint _queryCenter;
  Geoflutterfire geo = Geoflutterfire();

  BehaviorSubject<double> _boundFilter;
  StreamSubscription _boundSubscription;

  LatLngBounds currentBounds;

  double _meterPerPixel = 1;

  Set<Marker> markers;
  List<ClusterItem<T>> items;

  Set<Circle> circles;

  double get meterPerPixel => _meterPerPixel;
  void setMeterPerPixel(double zoom, double lat) {
    _meterPerPixel = 156543.03392 *
        cos(lat * pi / 180) /
        pow(2, 21 - zoom) /
        MediaQuery.of(context).devicePixelRatio;
    if (widget.metersPerPixelUpdate != null)
      widget.metersPerPixelUpdate(meterPerPixel);
  }

  _startQuery() async {
    if (widget.collection == null) return;
    _boundSubscription = _boundFilter.switchMap((rad) {
      return geo.collection(collectionRef: widget.collection).within(
          center: _queryCenter,
          radius: rad,
          field: 'position',
          strictMode: true);
    }).listen((data) async {
      if (widget.clusterBuilder != null && widget.enableClusters) {
        var clusters = await widget.clusterBuilder(data);
        if (clusters != null) _manager.setItems(clusters);
      } else {
        if (widget.markerBuilder != null) {
          var markers = await widget.markerBuilder(data);
          _updateMarkers(markers);
        }
        if (widget.circleBuilder != null) {
          var circles = await widget.circleBuilder(data);
          _updateCircles(circles);
        }
      }
    });
  }

  void updateMap() {
    _mapController.getVisibleRegion().then((bounds) {
      currentBounds = bounds;
      if (widget.currentBoundsUpdate != null)
        widget.currentBoundsUpdate(bounds);
      var center = _cameraPosition.target;

      if (widget.collection != null) {
        var ne = bounds.northeast;
        // Calculate radius (in meters).
        var rad = MapHelper.getDistanceFromLatLonInKm(center, ne);

        _queryCenter = GeoFirePoint(
            _cameraPosition.target.latitude, _cameraPosition.target.longitude);
        _boundFilter.add(rad);
      }
      if (widget.onCameraIdle != null) widget.onCameraIdle();
    });
  }

  @override
  void initState() {
    super.initState();
    if (widget.enableClusters) _manager = _initClusterManager();
    if (widget.collection != null)
      _boundFilter = BehaviorSubject<double>.seeded(5);
    _cameraPosition = widget.initPosition;
    if (widget.controller != null)
      widget.controller.addListener(() {
        _updateMarkers(widget.controller.value);
      });
  }

  @override
  void dispose() {
    super.dispose();
    _boundSubscription?.cancel();
    widget.controller?.dispose();
  }

  ClusterManager _initClusterManager() {
    var levels = List.generate(6, (i) => i * 2.0 + 1);
    return ClusterManager<T>(items, _updateMarkers,
        levels: levels,
        extraPercent: 0,
        markerBuilder: _clasterBuilder,
        initialZoom: widget.initPosition.zoom);
  }

  void _updateMarkers(Set<Marker> markers) {
    if (markers == null) return;
    print('Updated ${markers.length} markers');
    setState(() {
      this.markers = markers;
    });
  }

  void _updateCircles(Set<Circle> circles) {
    if (circles == null) return;
    setState(() {
      this.circles = circles;
    });
  }

  Future<Marker> _clasterBuilder(Cluster<T> cluster) async {
    return Marker(
        markerId: MarkerId(cluster.getId()),
        position: cluster.location,
        consumeTapEvents: true,
        onTap: () async {
          // print('---- $cluster');
          // cluster.items.forEach((p) => print(p));
          if (!cluster.isMultiple) {
            widget.onMarkerTap(cluster.items.first);
          } else {
            _mapController.animateCamera(CameraUpdate.newLatLngZoom(
                cluster.location, _cameraPosition.zoom + 2));
          }
        },
        icon: await (widget.clusterDrawer ?? _getMarkerBitmap)(cluster));
  }

  Future<BitmapDescriptor> _getMarkerBitmap(Cluster<T> cluster) async {
    var size = cluster.isMultiple ? 125 : 80;
    var text = cluster.isMultiple ? cluster.count.toString() : null;

    final PictureRecorder pictureRecorder = PictureRecorder();
    final Canvas canvas = Canvas(pictureRecorder);
    final Paint paint0 = Paint()..color = Colors.black26;
    final Paint paint1 = Paint()..color = Colors.orange;
    final Paint paint2 = Paint()..color = Colors.blue;
    final Paint paint3 = Paint()..color = Colors.white;
    canvas.drawCircle(
        Offset(size / 2, size / 2 + size / 20), size / 2.2, paint0);
    canvas.drawCircle(Offset(size / 2, size / 2), size / 2.2, paint1);
    canvas.drawCircle(Offset(size / 2, size / 2), size / 2.5, paint3);
    canvas.drawCircle(
        Offset(size / 2, size / 2), size / 3, text != null ? paint1 : paint2);

    if (text != null) {
      TextPainter painter = TextPainter(textDirection: TextDirection.ltr);
      painter.text = TextSpan(
        text: text,
        style: TextStyle(
            fontSize: size / 3,
            color: Colors.white,
            fontWeight: FontWeight.normal),
      );
      painter.layout();
      painter.paint(
        canvas,
        Offset(size / 2 - painter.width / 2, size / 2 - painter.height / 2),
      );
    }

    final img = await pictureRecorder.endRecording().toImage(size, size);
    final data = await img.toByteData(format: ImageByteFormat.png);

    return BitmapDescriptor.fromBytes(data.buffer.asUint8List());
  }

  @override
  Widget build(BuildContext context) {
    return GoogleMap(
      minMaxZoomPreference: widget.minMaxZoomPreference,
      initialCameraPosition: widget.initPosition,
      cameraTargetBounds: widget.maxBounds != null
          ? CameraTargetBounds(widget.maxBounds)
          : CameraTargetBounds.unbounded,
      markers: widget.markerBuilder == null && widget.markers != null
          ? widget.markers
          : markers,
      circles: widget.circleBuilder == null && widget.circles != null
          ? widget.circles
          : circles,
      onMapCreated: (GoogleMapController controller) async {
        _mapController = controller;
        if (widget.enableClusters) _manager.setMapController(controller);

        // _mapController.setMapStyle(_mapStyle);

        // _mapUpdateEvent.add(widget.markers);
        await Future.delayed(Duration(seconds: 2));
        // _getLocation();

        _queryCenter = GeoFirePoint(widget.initPosition.target.latitude,
            widget.initPosition.target.longitude);

        setMeterPerPixel(
            widget.initPosition.zoom, widget.initPosition.target.latitude);

        _startQuery();

        currentBounds = await controller.getVisibleRegion();

        if (widget.onMapCreated != null)
          widget.onMapCreated(_mapController, currentBounds, meterPerPixel);
      },
      onCameraMoveStarted: () {
        if (widget.onCameraStart != null) widget.onCameraStart();
      },
      onCameraIdle: () {
        updateMap();
        if (widget.enableClusters) _manager.updateMap();
      },
      tiltGesturesEnabled: false,
      rotateGesturesEnabled: false,
      zoomControlsEnabled: false,
      onCameraMove: (pos) async {
        _cameraPosition = pos;
        if (widget.enableClusters) _manager.onCameraMove(pos);

        setMeterPerPixel(pos.zoom, pos.target.latitude);

        if (widget.onCameraMove != null)
          widget.onCameraMove(pos, meterPerPixel);
      },
      compassEnabled: false,
      myLocationEnabled: true,
      mapToolbarEnabled: false,
      buildingsEnabled: true,
      myLocationButtonEnabled: false,
      trafficEnabled: false,
    );
  }
}
