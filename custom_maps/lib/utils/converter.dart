import 'dart:convert';
import 'dart:io';
import 'dart:math';

import 'package:flutter/material.dart';

class Converter {
  // static ImageProvider convertToImage(String base64, double size) {
  //   return base64 == null || base64.isEmpty
  //       ? null
  //       : Image.network(
  //           base64,
  //           width: size,
  //           height: size,
  //         ).image;
  // }

  static String imageToBase64(File file) {
    if (file == null) return null;
    List<int> imageBytes = file.readAsBytesSync();
    return base64Encode(imageBytes);
  }

  static double distance(Offset pos1, Offset pos2) {
    var dis = sqrt(pow(pos2.dx - pos1.dx, 2) + pow(pos2.dy - pos1.dy, 2));
    return dis;
  }
}
