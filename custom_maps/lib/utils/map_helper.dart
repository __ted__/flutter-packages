import 'dart:math';
import 'dart:ui';
import 'package:geoflutterfire/geoflutterfire.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'point.dart';
import 'crs.dart';
import 'converter.dart';

class MapHelper {
  static const double EarthRadius = 6378137.0;
  static const double DegreesToRadians = 0.0174532925;
  static const double RadiansToDegrees = 57.2957795;

  static LatLng findLatLnAtDistanceFrom(
      LatLng source, double bearing, double range) {
    double angularDistance = range * 1000 / EarthRadius;
    double latA = deg2rad(source.latitude);
    double lonA = deg2rad(source.longitude);
    double trueCourse = deg2rad(bearing);

    double lat = asin(sin(latA) * cos(angularDistance) +
        cos(latA) * sin(angularDistance) * cos(trueCourse));

    double dlon = atan2(sin(trueCourse) * sin(angularDistance) * cos(latA),
        cos(angularDistance) - sin(latA) * sin(lat));
    double lon = ((lonA + dlon + pi) % (pi * 2)) - pi;

    return LatLng(rad2deg(lat), rad2deg(lon));
  }

  static getDistanceFromLatLonInKm(LatLng pos1, LatLng pos2) {
    // var R = 6371; // Radius of the earth in km
    var dLat = deg2rad(pos2.latitude - pos1.latitude); // deg2rad below
    var dLon = deg2rad(pos2.longitude - pos1.longitude);
    var a = sin(dLat / 2) * sin(dLat / 2) +
        cos(deg2rad(pos1.latitude)) *
            cos(deg2rad(pos2.latitude)) *
            sin(dLon / 2) *
            sin(dLon / 2);
    var c = 2 * atan2(sqrt(a), sqrt(1 - a));
    var d = EarthRadius / 1000 * c; // Distance in km
    return d;
  }

  static deg2rad(deg) {
    return deg * (pi / 180);
  }

  static rad2deg(rad) {
    return rad * (180 / pi);
  }

  static Offset findPointAtDistanceFrom(
      Offset source, double angle, double range) {
    var x = range * cos(angle * DegreesToRadians) + source.dx;

    var y = range * sin(angle * DegreesToRadians) + source.dy;
    return Offset(x, y);
  }

  static double findDegreeAngle(Offset pos1, Offset pos2) {
    double dx = pos2.dx - pos1.dx;
    double dy = pos2.dy - pos1.dy;
    return atan2(dy, dx) * (180 / pi);
  }

  static double findRadianAngle(Offset pos1, Offset pos2) {
    double dx = pos2.dx - pos1.dx;
    double dy = pos2.dy - pos1.dy;
    var rad = atan2(dy, dx);
    return rad;
  }

  static Offset screenToLocation(
      {LatLng position, double zoom, LatLngBounds bounds}) {
    // if(bounds == null){
    //   bounds = await _mapController.getVisibleRegion();
    // }
    CustomPoint<num> southWestPoint =
        Epsg3857().latLngToPoint(bounds.southwest, zoom);
    CustomPoint<num> northEastPoint =
        Epsg3857().latLngToPoint(bounds.northeast, zoom);
    CustomPoint<num> markerPoint = Epsg3857().latLngToPoint(position, zoom);
    double x = markerPoint.x - southWestPoint.x;
    double y = markerPoint.y -
        northEastPoint.y; // + MediaQuery.of(context).size.height;
    // Point center = await _mapController.toScreenLocation(latLng);
    return Offset(x, y); // / MediaQuery.of(context).devicePixelRatio;
  }

  static LatLng locationToScreen(
      {Offset offset, double zoom, LatLngBounds bounds}) {
    CustomPoint<num> southWestPoint =
        Epsg3857().latLngToPoint(bounds.southwest, zoom);
    CustomPoint<num> northEastPoint =
        Epsg3857().latLngToPoint(bounds.northeast, zoom);

    return Epsg3857().pointToLatLng(
        CustomPoint(offset.dx + southWestPoint.x, offset.dy + northEastPoint.y),
        zoom);
    // Point center = await _mapController.toScreenLocation(latLng);
    // return Offset(point.x, point.y) / MediaQuery.of(context).devicePixelRatio;
  }

  static double calculateKmBetweenPoints(Offset radiusOff, Offset centerOff) {
    var kmRad = Converter.distance(radiusOff, centerOff) * 2;
    return kmRad;
  }

  static double calculateDistance(LatLng latLn1, LatLng latLn2) {
    if (latLn1 == null || latLn2 == null) return -1;
    var p = 0.017453292519943295;
    var c = cos;
    var a = 0.5 -
        c((latLn2.latitude - latLn1.latitude) * p) / 2 +
        c(latLn1.latitude * p) *
            c(latLn2.latitude * p) *
            (1 - c((latLn2.longitude - latLn1.longitude) * p)) /
            2;
    var dist = 12742 * asin(sqrt(a));

    return dist;
  }

  static String toGeoHash(LatLng pos) {
    return GeoFirePoint(pos.latitude, pos.longitude).hash;
  }
}
