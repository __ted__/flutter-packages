// import 'package:doers/Managers/user_manager.dart';
// import 'package:flutter_ui_templates/ui_widgets/base_page.dart';
// import 'package:flutter_ui_templates/ui_widgets/my_button.dart';
// import 'package:doers/Helper/extensions.dart';// import 'package:doers/Pages/Auth/register_base_page.dart';
// import 'package:doers/Utils/const.dart';
// import 'package:doers/master_view_base_page.dart';
// // import 'package:flutter_ui_templates/ui_widgets/sms_code_field.dart';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import 'package:flutter_ui_templates/ui_widgets/sms_code_field.dart';
import 'package:flutter_ui_templates/ui_widgets/my_button.dart';

typedef Future GetUserData();

// @optionalTypeArgs
mixin PhoneVerificationMixin<T extends StatefulWidget> on State<T> {
  @protected
  String get phoneNumber;
  @protected
  ValueChanged<FirebaseUser> get onSignIn;
  @protected
  GetUserData get onGetUserData;

  String verificationId;

  String _smsCode = '';

  int resendToken;

  @override
  @mustCallSuper
  void initState() {
    super.initState();
    verify();
  }

  Widget buildPage(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SMSCodeField(
            length: 6,
            onPinChange: (pin) {
              setState(() {
                this._smsCode = pin;
              });
            },
          ),
          SizedBox(
            height: 40,
          ),
          MyButton(
            'Next',
            onPressed: this._smsCode.length < 6
                ? null
                : () {
                    this.onSmsEnter(this._smsCode);
                  },
            backgroundColor: Colors.blue,
          ),
        ],
      ),
    );
  }

  Future onSmsEnter(String smsCode) {
    // Navigator.of(context).pop();
    final AuthCredential credential = PhoneAuthProvider.getCredential(
      verificationId: this.verificationId,
      smsCode: smsCode,
    );
    return _linkUser(credential);
  }

  void verify() async {
    final PhoneCodeAutoRetrievalTimeout autoRetrieve = (String verId) {
      this.verificationId = verId;
    };

    final PhoneCodeSent smsCodeSent = (String verId, [int forceCodeResend]) {
      this.verificationId = verId;
      print("ASDADSSDSADAS $forceCodeResend");
      resendToken = forceCodeResend;
      // smsCodeDialog(context).then((value) {
      //   print('Signed in');
      // });
    };

    final PhoneVerificationCompleted verifiedSuccess = (AuthCredential user) {
      print('verified');
      _linkUser(user);
    };

    final PhoneVerificationFailed verifiedFailed = (AuthException exc) {
      print('Failed ${exc.message}');
    };

    // await Future.delayed(Duration(seconds: 1));

    await FirebaseAuth.instance.verifyPhoneNumber(
        phoneNumber: phoneNumber,
        codeAutoRetrievalTimeout: autoRetrieve,
        codeSent: smsCodeSent,
        forceResendingToken: resendToken,
        timeout: const Duration(seconds: 60),
        verificationCompleted: verifiedSuccess,
        verificationFailed: verifiedFailed);
  }

  Future _linkUser(AuthCredential credential) async {
    FirebaseUser user = await FirebaseAuth.instance.currentUser();

    if (user == null) {
      return _signIn(credential);
    } else {
      try {
        var res = await user.linkWithCredential(credential);
        return onSignIn(res.user);
      } catch (e) {
        switch (e.code) {
          case "ERROR_CREDENTIAL_ALREADY_IN_USE":
            return _signIn(credential);
          default:
            print(e);
        }
      }
    }
  }

  Future _signIn(AuthCredential credential) async {
    try {
      var user = await FirebaseAuth.instance.signInWithCredential(credential);
      return onSignIn(user.user);
    } catch (e) {
      print(e);
    }
  }

  // onSignIn(AuthResult result) async {
  //   UserManager.fireUser = result.user;
  //   var userData = await UserManager.fetchUserData();
  //   if (userData != null) {
  //     Navigator.of(context).pushReplacement(
  //         MaterialPageRoute(builder: (BuildContext context) => MasterView()));
  //   } else {
  //     // Navigator.of(context).pop();
  //     Navigator.of(context).pushReplacement(MaterialPageRoute(
  //         builder: (BuildContext context) =>
  //             RegistrationPage(userId: result.user.uid)));
  //   }
  // }

}

// Column(
//         mainAxisSize: MainAxisSize.max,
//         mainAxisAlignment: MainAxisAlignment.center,
//         // crossAxisAlignment: CrossAxisAlignment.stretch,
//         children: <Widget>[
//           SMSCodeField(
//             length: 6,
//             onPinChange: (pin) {
//               setState(() {
//                 this.smsCode = pin;
//               });
//             },
//           ),
//           SizedBox(
//             height: 40,
//           ),
//           MyButton(
//             'next'.i18n,
//             onPressed: this.smsCode.length < 6
//                 ? null
//                 : () {
//                     // Navigator.of(context).pop();
//                     final AuthCredential credential =
//                         PhoneAuthProvider.getCredential(
//                       verificationId: this.verificationId,
//                       smsCode: this.smsCode,
//                     );
//                     _linkUser(credential);
//                   },
//             backgroundColor: mainColor,
//           ),
//         ],
//       ),
