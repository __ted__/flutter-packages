import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/widgets.dart';

import '../managers/app_localizations.dart';

extension StringExtentions on String {
  String get i18nn {
    var instance = AppLocalizations.instance;
    if (instance != null) return instance.translate(this);
    return this;
  }

  ImageProvider get getCachedImageProvider =>
      this.isEmpty || this == null ? null : CachedNetworkImageProvider(this);
}
