import 'dart:core';

import 'package:flutter/widgets.dart';

class BaseActivityStatus {
  static const BaseActivityStatus text = const BaseActivityStatus(1);
  static const BaseActivityStatus image = const BaseActivityStatus(2);

  final int code;

  const BaseActivityStatus(this.code);
  //todo add more statuses
}

class SeenStatus {
  static const int Sent = 0;
  static const int Recieved = 1;
  static const int Seen = 2;
  //todo add more statuses
}

class BaseActivityLog {
  String idFrom;
  String idTo;
  int activityStatus;
  int seenStatus;
  DateTime timestamp;
  String path;

  String documentID;

  BaseActivityLog(
      {@required this.activityStatus,
      @required this.idFrom,
      @required this.idTo,
      this.documentID}) {
    this.seenStatus = SeenStatus.Sent;
    // timestamp = DateTime.now();
  }

  BaseActivityLog.fromJson(String documentID, Map<String, dynamic> data) {
    this.documentID = documentID;
    idFrom = data['idFrom'];
    idTo = data['idTo'];
    seenStatus = data['seenStatus'];
    if (data.containsKey('timestamp') && data['timestamp'] != null)
      timestamp = data['timestamp'].toDate();
    else
      timestamp = DateTime.now();
    activityStatus = data['activityStatus'];
    // path = reference.path;
  }

  @mustCallSuper
  Map<String, Object> toJson() {
    Map<String, Object> json = new Map<String, Object>();
    json['idFrom'] = idFrom;
    json['idTo'] = idTo;
    json['seenStatus'] = seenStatus;
    json['activityStatus'] = activityStatus;
    return json;
  }
}
