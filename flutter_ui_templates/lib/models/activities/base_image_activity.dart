import 'base_activitylog.dart';

abstract class BaseImageActivity extends BaseActivityLog {
  String imagePath;
  String thumbPath;

  BaseImageActivity(
      {this.imagePath,
      this.thumbPath,
      String idFrom,
      String idTo,
      String documentID})
      : super(
            activityStatus: BaseActivityStatus.image.code,
            idFrom: idFrom,
            idTo: idTo,
            documentID: documentID);

  BaseImageActivity.fromJson(String documentID, Map<String, dynamic> data)
      : super.fromJson(documentID, data) {
    imagePath = data['imagePath'];
    thumbPath = data['thumbPath'];
  }

  @override
  Map<String, Object> toJson() {
    var json = super.toJson();
    json['imagePath'] = imagePath;
    json['thumbPath'] = thumbPath;

    return json;
  }
}
