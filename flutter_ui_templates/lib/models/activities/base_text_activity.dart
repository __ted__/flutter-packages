import 'base_activitylog.dart';

class BaseTextActivity extends BaseActivityLog {
  String text;

  BaseTextActivity({this.text, String idFrom, String idTo, String documentID})
      : super(
            activityStatus: BaseActivityStatus.text.code,
            idFrom: idFrom,
            idTo: idTo,
            documentID: documentID);

  BaseTextActivity.fromJson(String documentID, Map<String, dynamic> data)
      : super.fromJson(documentID, data) {
    text = data['text'];
  }

  @override
  Map<String, Object> toJson() {
    var json = super.toJson();
    json['text'] = text;
    return json;
  }
}
