import 'dart:core';

class BaseUser {
  String uid;
  String name;
  String lastName;
  String email;
  String image;

  bool online;

  String get fullName => '$name $lastName';

  BaseUser({this.name, this.lastName, this.email, this.image});
}
