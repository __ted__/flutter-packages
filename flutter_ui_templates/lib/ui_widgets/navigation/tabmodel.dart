import 'package:flutter/material.dart';

import '../base_page.dart';

class TabModel {
  BasePage page;
  String name;
  IconData icon;
  GlobalKey<NavigatorState> navigator;

  bool selected = false;

  Color backgroundColor;
  Color selectedBackgroundColor;

  TabModel(
      {@required this.page,
      @required this.icon,
      @required this.name,
      this.backgroundColor = Colors.white,
      this.selectedBackgroundColor = Colors.white}) {
    this.navigator = GlobalKey<NavigatorState>();
  }
}
