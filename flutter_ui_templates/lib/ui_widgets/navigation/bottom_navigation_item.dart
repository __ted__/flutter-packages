import 'package:flutter/material.dart';

import 'tabmodel.dart';

abstract class MyBottomNavigationItem {
  TabModel model;
  MyBottomNavigationItem({this.model});
  BottomNavigationBarItem buildItem();
}
