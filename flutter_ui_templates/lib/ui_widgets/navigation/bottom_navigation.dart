import 'package:flutter/material.dart';
import 'bottom_navigation_item.dart';

class BottomNavigation extends StatefulWidget {
  BottomNavigation(
      {this.currentTab,
      this.onSelectTab,
      this.tabs,
      this.showSelectedLabels = false,
      this.showUnselectedLabels = false});
  final MyBottomNavigationItem currentTab;
  final List<MyBottomNavigationItem> tabs;
  final ValueChanged<int> onSelectTab;

  final bool showSelectedLabels;
  final bool showUnselectedLabels;

  static BottomNavigationState of(BuildContext context) {
    var state = context.findAncestorStateOfType<BottomNavigationState>();
    return state;
  }

  @override
  BottomNavigationState createState() => BottomNavigationState();
}

class BottomNavigationState extends State<BottomNavigation> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
              offset: Offset(0, -2), blurRadius: 8, color: Color(0x33333333)),
        ],
      ),
      // height: 70,// bottom navigation bar height
      child: BottomNavigationBar(
        iconSize: 30,
        backgroundColor: Colors.white,
        type: BottomNavigationBarType.fixed,
        items: widget.tabs.map<BottomNavigationBarItem>((f) {
          return f.buildItem();
        }).toList(),
        currentIndex: widget.tabs.indexOf(widget.currentTab),
        onTap: (index) => widget.onSelectTab(index),
        showSelectedLabels: widget.showSelectedLabels,
        showUnselectedLabels: widget.showUnselectedLabels,
        selectedFontSize: 10,
        unselectedFontSize: 10,
      ),
    );
  }
}
