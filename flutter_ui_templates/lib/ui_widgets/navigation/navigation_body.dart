import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:flutter_ui_templates/ui_widgets/base_page.dart';
import 'package:flutter_ui_templates/ui_widgets/navigation/tabmodel.dart';

import 'bottom_navigation.dart';
import 'bottom_navigation_item.dart';

typedef void TabChange(TabModel previous, TabModel next);

class NavigationBody extends StatefulWidget {
  final List<MyBottomNavigationItem> pages;
  final bool showSelectedLabels;
  final bool showUnselectedLabels;
  final int initPage;
  final TabChange onTabChange;

  NavigationBody(
      {this.pages,
      this.initPage = 0,
      this.onTabChange,
      this.showSelectedLabels,
      this.showUnselectedLabels});
  @override
  _NavigationBodyState createState() => _NavigationBodyState();
  static NavigatorState selectTab(BuildContext context, int index,
      {bool closeAll = false}) {
    _NavigationBodyState state =
        context.findAncestorStateOfType<_NavigationBodyState>();
    if (closeAll)
      state.widget.pages[index].model.navigator.currentState.popUntil((x) {
        return x.isFirst;
      });
    return state._selectTab(index);
  }

  static State of(BuildContext context) {
    return context.findAncestorStateOfType<_NavigationBodyState>();
  }
}

class _NavigationBodyState extends State<NavigationBody> {
  Queue<int> tabQueue = new Queue<int>();
  MyBottomNavigationItem get currentTab => widget.pages[tabQueue.last];

  @override
  void initState() {
    super.initState();
    tabQueue.add(widget.initPage);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async {
          bool canpop =
              await currentTab.model.navigator.currentState.maybePop();
          if (!canpop) {
            _removeTab();
            canpop = tabQueue.length > 0;
          }
          return !canpop;
        },
        child: Scaffold(
            resizeToAvoidBottomInset: true,
            resizeToAvoidBottomPadding: true,
            body: Stack(
                fit: StackFit.expand,
                children: widget.pages.map<Widget>((tab) {
                  return _buildOffstageNavigator(tab);
                }).toList()),
            bottomNavigationBar: BottomNavigation(
              currentTab: currentTab,
              tabs: widget.pages,
              showSelectedLabels: widget.showSelectedLabels,
              showUnselectedLabels: widget.showUnselectedLabels,
              onSelectTab: _selectTab,
            )));
  }

  Widget _buildOffstageNavigator(MyBottomNavigationItem tab) {
    return Offstage(
        offstage: currentTab != tab,
        child: Navigator(
            key: tab.model.navigator,
            // initialRoute: TabNavigatorRoutes.root,
            onGenerateRoute: (routeSettings) {
              return MaterialPageRoute(builder: (context) => tab.model.page);
            }));
  }

  NavigatorState _selectTab(int tabItem) {
    widget.pages.forEach((x) => x.model.selected = false);
    widget.pages[tabItem].model.selected = true;
    if (widget.onTabChange != null)
      widget.onTabChange(currentTab.model, widget.pages[tabItem].model);
    if (currentTab == widget.pages[tabItem]) {
      // while (widget.pages[tabItem].navigator.currentState.canPop()) {
      //   widget.pages[tabItem].navigator.currentState.pop();
      // }
      widget.pages[tabItem].model.navigator.currentState.popUntil((x) {
        return x.isFirst;
      });
    } else
      tabQueue.add(tabItem);
    setState(() {});
    return widget.pages[tabItem].model.navigator.currentState;
  }

  void _removeTab() {
    if (tabQueue.length > 1) {
      setState(() {
        tabQueue.removeLast();
      });
    }
  }
}
