import 'package:flutter/material.dart';

class MyPageAction {
  String title;
  Function action;
  IconData icon;

  MyPageAction(
      {@required this.title, this.action, this.icon = Icons.more_vert});
}
