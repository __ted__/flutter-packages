import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class MyBackgroundContainer extends StatelessWidget {
  final Widget child;
  final EdgeInsets padding;
  final BoxConstraints boxConstraints;

  final String background;
  // final bool isOverTab;

  const MyBackgroundContainer({
    Key key,
    this.child,
    this.padding = EdgeInsets.zero,
    this.background,
    this.boxConstraints,
    // this.isOverTab=false
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: boxConstraints,
      // constraints: BoxConstraints(minHeight: minheight, maxHeight: maxHeight),
      decoration: this.background == null
          ? null
          : BoxDecoration(
              image: DecorationImage(
                  repeat: ImageRepeat.repeat,
                  alignment: Alignment.topCenter,
                  // fit: BoxFit.fitHeight,
                  image: AssetImage(this.background))),
      padding: this.padding,
      child: this.child,
    );
  }
}
