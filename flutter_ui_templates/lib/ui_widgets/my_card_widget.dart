import 'package:flutter/material.dart';

class MyCard extends StatelessWidget {
  final Widget child;
  final Color color;
  final double elevation;
  final double radius;

  final EdgeInsets padding;
  final bool outline;

  const MyCard(
      {Key key,
      this.child,
      this.padding,
      this.elevation = 3.0,
      this.radius = 6.0,
      this.color = Colors.white,
      this.outline = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
        margin: EdgeInsets.symmetric(vertical: 4),
        shape: RoundedRectangleBorder(
            side: BorderSide(
                color: Colors.red,
                width: 1,
                style: outline ? BorderStyle.solid : BorderStyle.none),
            borderRadius: BorderRadius.circular(radius)),
        color: color,
        elevation: elevation,
        child: Padding(
          padding: padding ?? const EdgeInsets.all(6.0),
          child: this.child,
        ));
  }
}
