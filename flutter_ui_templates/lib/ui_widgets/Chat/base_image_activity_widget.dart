import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:flutter_ui_templates/library/localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_ui_templates/models/activities/base_image_activity.dart';
import 'package:flutter_ui_templates/ui_widgets/gallery/gallery_photo_view.dart';
import 'package:flutter_ui_templates/ui_widgets/gallery/gallery_view_item.dart';

import 'base_chat_page.dart';

class BaseImageActivityWidget extends StatelessWidget {
  final BaseImageActivity imageActivity;
  final List<GalleryViewItem> images;
  final Widget loadingWidget;
  const BaseImageActivityWidget({
    Key key,
    this.imageActivity,
    this.images,
    this.loadingWidget,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double imageMaxWidth = MediaQuery.of(context).size.width / 2;
    GalleryViewItem item = images.firstWhere((x) {
      var splito = imageActivity.path.split('/');
      return x.id == splito.last;
    }, orElse: () => GalleryViewItem(id: 'hero', url: '', thumbnail: ''));
    return GestureDetector(
      child: Hero(
        tag: item.id,
        child: CachedNetworkImage(
          placeholder: (context, url) =>
              this.loadingWidget ?? Center(child: CircularProgressIndicator()),
          width: imageMaxWidth,
          fadeOutDuration: Duration.zero,
          fit: BoxFit.fitHeight,
          imageUrl: item.thumbnail ?? item.url,
        ),
      ),
      onTap: () async {
        var res = await Navigator.push(
            context,
            PageRouteBuilder(
                transitionDuration: Duration(milliseconds: 500),
                pageBuilder: (_, __, ___) => GalleryPhotoViewWrapper(
                      galleryItems: images,
                      canEdit: true,
                      canShare: true,
                      pictureText: 'picture',
                      backgroundDecoration: const BoxDecoration(
                        color: Colors.black,
                      ),
                      loadingChild: loadingWidget ??
                          Center(
                            child: CircularProgressIndicator(),
                          ),
                      initialIndex: images.indexOf(item),
                    )));
        String url = res[0];
        String method = res[1];
        // edit image
        if (method == 'edit') {
          if (url != null && url != "") {
            ChatScreenMixin chat = BaseChat.of(context);
            chat.setLoading = true;

            var cache = await DefaultCacheManager().getSingleFile(url);

            var data = await cache.readAsBytes();

            await chat.editAndUpload([data]);
          }
        }
        if (method == 'share') {
          if (url != null && url != "") {
            ChatScreenMixin chat = BaseChat.of(context);
            chat.setLoading = true;
            var cache = await DefaultCacheManager().getSingleFile(url);

            var data = await cache.readAsBytes();
            await chat.shareImage(data);
          }
        }
        if (method == 'download') {
          if (url != null && url != "") {
            ChatScreenMixin chat = BaseChat.of(context);
            chat.setLoading = true;
            var cache = await DefaultCacheManager().getSingleFile(url);

            var data = await cache.readAsBytes();
            await chat.downloadImage(data);
          }
        }
        // MaterialPageRoute(

        //   // fullscreenDialog: true,
        //   builder: (context) => GalleryPhotoViewWrapper(
        //         galleryItems: images,
        //         backgroundDecoration: const BoxDecoration(
        //           color: Colors.black,
        //         ),
        //         initialIndex: images.indexOf(item),
        // ))
        // MasterViewState.instance.bottomNavigationEnabled = true;
      },
    );
  }
}
