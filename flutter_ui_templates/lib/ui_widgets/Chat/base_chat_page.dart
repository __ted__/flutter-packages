import 'dart:async';
import 'dart:math';
import 'dart:typed_data';

import 'package:cached_network_image/cached_network_image.dart';

import 'package:flutter_ui_templates/models/activities/base_activitylog.dart';
import 'package:flutter_ui_templates/models/activities/base_image_activity.dart';
import 'package:flutter_ui_templates/models/activities/base_text_activity.dart';
import 'package:flutter_ui_templates/models/user_model.dart';
import 'package:flutter_ui_templates/ui_widgets/base_page.dart';
import 'package:flutter_ui_templates/ui_widgets/draw_page.dart';
import 'package:flutter/material.dart';

import 'package:flutter_ui_templates/ui_widgets/gallery/gallery_previewer.dart';
import 'package:flutter_ui_templates/ui_widgets/gallery/gallery_view_item.dart';

import 'package:flutter_ui_templates/ui_widgets/my_page.dart';

import 'chat_content.dart';

abstract class BaseChat extends BasePage {
  // final String userId;
  final BaseUser peer;
  final String path;
  final ValueChanged<String> onSeen;

  static String currentChatPath;

  BaseChat({
    Key key,
    // @required this.userId,
    @required this.peer,
    @required this.path,
    @required this.onSeen,
  }) : super(key: key);

  static ChatScreenMixin of(BuildContext context) {
    return context.findAncestorStateOfType<ChatScreenMixin>();
  }
}

mixin ChatScreenMixin<T extends BaseChat> on State<T> {
  // String widget.peerId;
  // String peerAvatar;
  String userId;

  bool isUserTyping = false;
  bool isPeerTyping = false;

  // File imageFile;
  bool _isLoading;
  bool canSend = false;

  ImageProvider userImage;

  int messageLimit = 20;
  int get limitStep => 10;
  bool get showImage => true;
  bool get isLoading => _isLoading;

  set setLoading(bool loading) {
    setState(() {
      _isLoading = loading;
    });
  }

  StreamSubscription get typingSubscription;
  StreamSubscription get imagesSubscription;

  List<GalleryViewItem> images;
  bool showGallery = false;
  final ScrollController galleryGridController = new ScrollController();

  final TextEditingController textEditingController =
      new TextEditingController();
  final ScrollController listScrollController = new ScrollController();
  final FocusNode focusNode = new FocusNode();
  Color randomColor =
      Color((Random().nextDouble() * 0xFFFFFF).toInt() << 0).withOpacity(1.0);

  @override
  @mustCallSuper
  void initState() {
    super.initState();
    BaseChat.currentChatPath = widget.path;

    focusNode.addListener(_onFocusChange);
    canSend = false;
    userImage = CachedNetworkImageProvider(widget.peer?.image);
    listScrollController.addListener(_scrollListener);
    galleryGridController.addListener(() {
      if (showGallery) {
        if (galleryGridController.offset < -50) {
          setState(() {
            showGallery = false;
          });
        }
      }
    });

    images = new List<GalleryViewItem>();
    _isLoading = true;
  }

  @override
  @mustCallSuper
  void dispose() {
    super.dispose();
    BaseChat.currentChatPath = "";

    typingSubscription?.cancel();
    imagesSubscription?.cancel();
  }

  // getTyping();

  _scrollListener() {
    if (listScrollController.offset >=
            listScrollController.position.maxScrollExtent &&
        !listScrollController.position.outOfRange) {
      setState(() {
        print("reach the top");
        messageLimit += limitStep;
      });
    }
    if (listScrollController.offset <=
            listScrollController.position.minScrollExtent &&
        !listScrollController.position.outOfRange) {
      // setState(() {});
    }
  }

  _onFocusChange() {
    if (focusNode.hasFocus) {
      // Hide sticker when keyboard appear
      setState(() {
        canSend = false;
        if (showGallery) showGallery = false;
      });
    }
  }

  // Future getImage(ImageSource source) async {
  //   File imageFile = await ImagePicker.pickImage(source: source);

  //   if (imageFile != null) {
  //     setLoading = true;

  //     Uint8List imageData = await imageFile.readAsBytes();

  //     imageData = await editPhoto(imageData);

  //     uploadFiles(imageData);
  //     // onError: (err) {
  //     //   setState(() {
  //     //     _isLoading = false;
  //     //   });
  //     //   Fluttertoast.showToast(msg: 'This file is not an image');
  //     // });
  //   }
  // }

  Future<Uint8List> editPhoto(Uint8List imageData) async {
    return Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => Draw(
                  imageData: imageData,
                  loadingWidget: buildLoading(),
                )));
  }

  void uploadFiles(List<Uint8List> data) {
    if (data != null) {
      onSendMessage('_', data);
    }
    setState(() {
      _isLoading = false;
      showGallery = false;
    });
  }

  Future editAndUpload(List<Uint8List> data) async {
    setLoading = true;
    List<Uint8List> _data = [];
    if (data != null && data.length == 1) {
      var edited = await editPhoto(data[0]);
      if (edited == null) {
        setLoading = false;
        return;
      }
      _data.add(edited);
    } else
      _data = data;
    uploadFiles(_data);
  }

  @mustCallSuper
  Future<bool> onSendMessage(String text, List<Uint8List> imageFiles) async {
    // type: 0 = text, 1 = image, 2 = sticker
    if (text.trim() != '') {
      textEditingController.clear();
      setState(() {
        canSend = false;
      });

      listScrollController.animateTo(0.0,
          duration: Duration(milliseconds: 300), curve: Curves.easeOut);
      return true;
    } else {
      return false;
      // Fluttertoast.showToast(msg: 'Nothing to send');
    }
  }

  @mustCallSuper
  Widget buildItem(int i, List<BaseActivityLog> messages) {
    BaseActivityLog document = messages[max(i, 0)];
    if (document == null ||
        (document is BaseTextActivity || document is BaseImageActivity)) {
      return ChatActivityWidget(
          activityLog: document,
          i: i,
          listMessage: messages,
          userId: userId,
          peer: widget.peer,
          peerImage: userImage,
          isPeerTyping: isPeerTyping,
          images: images,
          loadingWidget: buildLoading(),
          onSeen: widget.onSeen);
    }
    return null;
  }

  Future<bool> onBackPress() {
    if (showGallery) {
      setState(() {
        showGallery = false;
      });
      return Future.value(false);
    }
    if (canSend) {
      setState(() {
        canSend = false;
      });
    }
    setTyping(false, () {});

    Navigator.of(context).pop();

    return Future.value(false);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: MyPage(
      background: 'assets/BGR.jpg',
      onBackPressed: onBackPress,
      actions: [],
      child: WillPopScope(
        child: Stack(
          children: <Widget>[
            StreamBuilder(
                stream: getStream(),
                builder: (context, snapshot) {
                  if (!snapshot.hasData) {
                    return buildLoading();
                  } else if (snapshot.data.documents.length == 0) {
                    return Center(child: Text("Welcome"));
                  } else {
                    return Column(
                      children: <Widget>[
                        Expanded(child: buildListMessage(snapshot)),
                        buildInput(),
                        buildGalleryPreviewer(),
                      ],
                    );
                  }
                }),

            // Loading
            buildLoading()
          ],
        ),
        onWillPop: onBackPress,
      ),
    ));
  }

  setTyping(bool istyping, Function _func);
  Widget buildLoading();

  Widget buildInput();

  Widget buildListMessage(AsyncSnapshot<dynamic> snapshot);
  Stream getStream();

  Widget buildGalleryPreviewer() {
    return GalleryPreviewer(
      collapsed: !showGallery,
      controller: galleryGridController,
      duration: Duration(milliseconds: 200),
      onTap: (data) {
        setLoading = true;
        editAndUpload(data);
      },
    );
  }

  getChatIndex(int index) {
    return max(index - (isPeerTyping ? 1 : 0), 0);
  }

  shareImage(Uint8List url){}
  downloadImage(Uint8List url){}
}
