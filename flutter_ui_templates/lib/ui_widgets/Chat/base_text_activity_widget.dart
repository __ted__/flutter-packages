import 'package:flutter/material.dart';
import 'package:flutter_ui_templates/models/activities/base_text_activity.dart';

class BaseTextActivityWidget extends StatelessWidget {
  final BaseTextActivity textActivity;
  final bool isMe;
  const BaseTextActivityWidget({Key key, this.textActivity, this.isMe})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    double textMaxWidth = MediaQuery.of(context).size.width * 2 / 3;
    return Container(
      constraints: BoxConstraints(maxWidth: textMaxWidth),

      child: Text(
        textActivity.text,
        textAlign: TextAlign.left,
        style: TextStyle(
            fontSize: 16,
            color: isMe ? Colors.white : Colors.black,
            fontFamily: 'roboto'),
      ),
      padding: EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 10.0),
      // width: hasImage ? 300.0 : double,
      // decoration: BoxDecoration(
      //   //   boxShadow: [
      //   //     BoxShadow(
      //   //         color: Colors.grey, offset: Offset(1.5, 0.5))
      //   //   ],
      // ),
    );
  }
}
