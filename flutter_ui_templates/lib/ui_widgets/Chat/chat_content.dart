import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_ui_templates/models/activities/base_activitylog.dart';
import 'package:flutter_ui_templates/models/activities/base_image_activity.dart';
import 'package:flutter_ui_templates/models/activities/base_text_activity.dart';
import 'package:flutter_ui_templates/models/user_model.dart';
import 'package:flutter_ui_templates/ui_widgets/Chat/base_text_activity_widget.dart';
import 'package:flutter_ui_templates/ui_widgets/gallery/gallery_view_item.dart';
import 'package:intl/intl.dart';

import 'chat_avatar.dart';
import 'base_image_activity_widget.dart';

class ChatActivityWidget extends StatelessWidget {
  final List<BaseActivityLog> listMessage;
  final int i;
  final bool isPeerTyping;
  final BaseUser peer;
  final ImageProvider peerImage;
  final String userId;
  final BaseActivityLog activityLog;
  final Widget loadingWidget;
  final List<GalleryViewItem> images;

  final Radius corner = const Radius.circular(18.0);
  final Radius flat = const Radius.circular(5.0);
  final typingGif = const AssetImage('assets/typing3.gif');
  final ValueChanged<String> onSeen;

  const ChatActivityWidget(
      {Key key,
      this.activityLog,
      this.i,
      this.listMessage,
      this.userId,
      this.peer,
      this.peerImage,
      this.isPeerTyping,
      this.images,
      this.loadingWidget,
      this.onSeen})
      : super(key: key);

  bool get typingWidget => i == -1 && isPeerTyping;
  bool get isMe => activityLog.idFrom == this.userId && !typingWidget;

  @override
  Widget build(BuildContext context) {
    var index = getChatIndex(i);

    if (!isMe && activityLog.seenStatus == SeenStatus.Recieved) {
      print("OLA");
      activityLog.seenStatus = SeenStatus.Seen;
      this.onSeen(activityLog.path);
    }

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
            padding: EdgeInsets.only(
                left: 10.0,
                right: 10.0,
                bottom: isMe && isLastMessageRight(index) ? 20.0 : 5),
            child: Row(
                mainAxisAlignment:
                    isMe ? MainAxisAlignment.end : MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.end,
                // mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  //avatar
                  ChatAvatar(
                    showAvatar: !isMe && isLastMessageLeft(index),
                    peer: peer,
                    userImage: peerImage,
                  ),
                  Material(
                      color: isMe && activityLog is BaseTextActivity
                          ? Colors.blue
                          : Theme.of(context).cardColor,
                      borderRadius: BorderRadius.only(
                        topLeft:
                            (isMe) || (!isMe && isLastMessageLeft(index + 2))
                                ? corner
                                : flat,
                        topRight:
                            (isMe && isLastMessageRight(index + 2)) || (!isMe)
                                ? corner
                                : flat,
                        bottomRight:
                            isMe && isLastMessageRight(index) || (!isMe)
                                ? corner
                                : flat,
                        bottomLeft: isMe ||
                                (!isMe &&
                                    isLastMessageLeft(index) &&
                                    !(isPeerTyping && index != i))
                            ? corner
                            : flat,
                      ),
                      elevation: 2,
                      clipBehavior: Clip.hardEdge,
                      child: typingWidget
                          ? Container(
                              padding: EdgeInsets.symmetric(horizontal: 10),
                              child: Image(
                                image: typingGif,
                                height: 40,
                                // fit: BoxFit.contain,
                              ))
                          : _drawMessage(activityLog, isMe)),

                  //seen
                  isMe
                      ? Icon(
                          activityLog.seenStatus == SeenStatus.Sent
                              ? Icons.check_circle_outline
                              : Icons.check_circle,
                          size: 12,
                          color: activityLog.seenStatus < SeenStatus.Seen
                              ? Colors.blue
                              : Colors.transparent)
                      : Container()
                ])),

        // Time
        !isMe &&
                isLastMessageLeft(index) &&
                !typingWidget &&
                activityLog.timestamp != null
            ? Container(
                child: Text(
                  DateFormat('dd MMM kk:mm').format(activityLog.timestamp),
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 12.0,
                      fontStyle: FontStyle.italic),
                ),
                margin: EdgeInsets.only(left: 5.0, top: 5.0, bottom: 5.0),
              )
            : Container()
      ],
    );
  }

  getChatIndex(int index) {
    return max(index - (isPeerTyping ? 1 : 0), 0);
  }

  bool isLastMessageLeft(int index) {
    if ((index > 0 &&
            listMessage != null &&
            listMessage[min(index, listMessage.length) - 1].idFrom ==
                this.userId) ||
        index == 0) {
      return true;
    } else {
      return false;
    }
  }

  bool isLastMessageRight(int index) {
    if ((index > 0 &&
            listMessage != null &&
            listMessage[min(index, listMessage.length) - 1].idFrom !=
                this.userId) ||
        index == 0) {
      return true;
    } else {
      return false;
    }
  }

  _drawMessage(BaseActivityLog activityLog, bool isMe) {
    if (activityLog is BaseImageActivity) {
      return BaseImageActivityWidget(
        imageActivity: activityLog,
        images: images,
        loadingWidget: loadingWidget,
      );
    } else {
      return BaseTextActivityWidget(
          textActivity: activityLog as BaseTextActivity, isMe: isMe);
    }
  }
}
