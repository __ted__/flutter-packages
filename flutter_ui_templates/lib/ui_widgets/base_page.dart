import 'package:flutter/widgets.dart';

abstract class BasePage extends StatefulWidget {
  const BasePage({Key key}) : super(key: key);
  String get name;
}
