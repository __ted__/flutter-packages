import 'dart:async';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';

class PresenceWidget extends StatefulWidget {
  final String onlineText;
  final String offlineText;
  PresenceWidget({@required this.onlineText, @required this.offlineText});

  @override
  _PresenceWidgetState createState() => _PresenceWidgetState();
}

class _PresenceWidgetState extends State<PresenceWidget> {
  ConnectivityResult result;
  StreamSubscription<ConnectivityResult> presenceListener;

  @override
  void initState() {
    super.initState();
    presenceListener = Connectivity().onConnectivityChanged.listen((result) {
      setState(() {
        this.result = result;
      });

      // switch (result) {
      //   case ConnectivityResult.none:
      // return AnimatedContainer(
      //   // default title
      //   child: Text(
      //     'Please check your internet connection',
      //     style: TextStyle(color: Colors.white, fontSize: 14),
      //   ),
      //   color: Colors.redAccent, // default color
      //   duration: Duration(milliseconds: 400),
      // );
      //     break;
      //   default:
      //     return
      // }
    });
  }

  @override
  void dispose() {
    super.dispose();
    presenceListener?.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        color:
            result == ConnectivityResult.none ? Colors.redAccent : Colors.green,
        child: AnimatedContainer(
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.all(2),
          constraints: BoxConstraints(
              maxHeight: result == ConnectivityResult.none ? 100 : 0),
          // default title
          child: Text(
            (result == ConnectivityResult.none
                ? widget.offlineText
                : widget.onlineText),
            textAlign: TextAlign.center,
            style: TextStyle(color: Colors.white, fontSize: 14),
          ),

          duration:
              Duration(seconds: result == ConnectivityResult.none ? 5 : 2),
        ),
      ),
    );
  }
}
