import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'my_background_container.dart';
import 'my_page_action_model.dart';

class MyPage extends StatelessWidget {
  final EdgeInsets padding;
  final ScrollPhysics physics;
  final Widget child;
  final List<MyPageAction> actions;
  final VoidCallback onBackPressed;
  final String background;
  final Color backgroundColor;
  final bool scrollable;
  final bool canPop;

  MyPage({
    this.padding = EdgeInsets.zero,
    this.physics,
    this.child,
    this.actions,
    this.onBackPressed,
    this.background,
    this.backgroundColor,
    this.scrollable = true,
    this.canPop = true,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backgroundColor,
      body: LayoutBuilder(builder: (_, cc) {
        BoxConstraints constraints = cc;
        if (scrollable)
          constraints =
              cc.copyWith(minHeight: cc.maxHeight, maxHeight: double.infinity);
        return Stack(
          children: <Widget>[
            scrollable
                ? SingleChildScrollView(
                    padding: padding,
                    physics: physics ?? ClampingScrollPhysics(),
                    child: MyBackgroundContainer(
                      child: child,
                      // height: height,
                      boxConstraints: constraints,
                      background: background,
                    ))
                : MyBackgroundContainer(
                    child: child,
                    background: background,
                    boxConstraints: constraints,
                  ),
            Container(
                // height: 130,
                // padding: EdgeInsets.only(bottom: 10),
                decoration: BoxDecoration(
                    // backgroundBlendMode: BlendMode.srcOut,
                    gradient: LinearGradient(
                        colors: [Colors.black26, Colors.transparent],
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        stops: [0, 1])),
                child: SafeArea(
                  child: Container(
                      padding: EdgeInsets.only(left: 10, right: 10),
                      // constraints: BoxConstraints(maxHeight: 100),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          canPop
                              ?
                              // Hero(
                              //     tag: "back_button",
                              // child:
                              RawMaterialButton(
                                  constraints: BoxConstraints(
                                      maxWidth: 60, maxHeight: 60),
                                  onPressed: () {
                                    if (onBackPressed != null) {
                                      onBackPressed();
                                    } else
                                      Navigator.of(context).maybePop();
                                  },
                                  child: new Icon(
                                    Icons.arrow_back,
                                    color: Colors.grey,
                                    // size: 20.0,
                                  ),
                                  shape: new CircleBorder(),
                                  elevation: 2.0,
                                  fillColor: Colors.white,
                                  padding: const EdgeInsets.all(10.0),
                                  // )
                                )
                              : SizedBox(
                                  height: 0,
                                ),
                          // if (actions != null && actions.length == 1)
                          //   RawMaterialButton(
                          //     constraints:
                          //         BoxConstraints(maxWidth: 60, maxHeight: 60),
                          //     // onPressed: null,
                          //     child: new Icon(
                          //       actions[0].icon,
                          //       color: Colors.grey,
                          //       // size: 25.0,
                          //     ),
                          //     shape: new CircleBorder(),
                          //     elevation: 2.0,
                          //     onPressed: () {
                          //       actions[0].action();
                          //     },
                          //     fillColor: Colors.white,
                          //     padding: const EdgeInsets.all(10.0),
                          //   )
                          // else
                          if (actions != null)
                            ConstrainedBox(
                              constraints: BoxConstraints(maxHeight: 60),
                              child: DropdownButton<MyPageAction>(
                                underline: SizedBox(),
                                icon: AbsorbPointer(
                                  child: RawMaterialButton(
                                    constraints: BoxConstraints(
                                        maxWidth: 60, maxHeight: 60),
                                    // onPressed: null,
                                    child: new Icon(
                                      Icons.more_vert,
                                      color: Colors.grey,
                                      // size: 25.0,
                                    ),
                                    shape: new CircleBorder(),
                                    elevation: 2.0, onPressed: () {},
                                    fillColor: Colors.white,
                                    padding: const EdgeInsets.all(10.0),
                                  ),
                                ),
                                // iconSize: 24,
                                // elevation: 16,
                                // style: TextStyle(color: Colors.deepPurple),
                                onChanged: (MyPageAction myaction) {
                                  myaction.action();
                                },
                                items: actions
                                    .map<DropdownMenuItem<MyPageAction>>(
                                        (MyPageAction value) {
                                  return DropdownMenuItem<MyPageAction>(
                                    value: value,
                                    child: Text(value.title),
                                  );
                                }).toList(),
                              ),
                            )
                          else
                            SizedBox(
                              height: 0,
                            )
                        ],
                      )),
                )),
          ],
        );
      }),
    );
  }
}
